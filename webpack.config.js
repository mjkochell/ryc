require('dotenv').config();
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const RemoveStrictPlugin = require('remove-strict-webpack-plugin');

const path = require('path');

const HTMLPlugin = require('html-webpack-plugin');
const DotenvPlugin = require('dotenv-webpack');
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');

module.exports = {
    entry: [
        './src/index.tsx',
    ],
    mode: 'development',
    resolve: {
        modules: [
            'src',
            'node_modules',
        ],
        extensions: ['*', '.js', '.jsx', '.ts', '.tsx'],
    },
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './dist',
        historyApiFallback: {
            index: 'index.html',
        },
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': '*',
        },
    },
    output: {
        path: path.join(__dirname, '/dist'),
        publicPath: '/',
        filename: '[name].[contenthash].js',
    },
    plugins: [
        new HTMLPlugin({
            hash: true,
            alwaysWriteToDisk: true,
            inject: 'body',
            title: 'My Awesome application',
            myPageHeader: 'Hello World',
            template: './src/index.ejs',
            filename: 'index.html',
            templateParameters: {
                PLACES_API_KEY: process.env.PLACES_API_KEY,
            },
        }),
        new DotenvPlugin(),
        new RemoveStrictPlugin(),
        new HtmlWebpackHarddiskPlugin({
            outputPath: path.join(__dirname, '/dist'),
        }),
        // new BundleAnalyzerPlugin(),
    ],
    module: {
        rules: [
            {
                test: /\.(js|jsx|ts|tsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        plugins: [
                            '@babel/plugin-proposal-class-properties',
                            '@babel/plugin-syntax-dynamic-import',
                            '@babel/proposal-object-rest-spread',
                            'babel-plugin-typescript-to-proptypes',
                        ],
                        presets: [
                            ['@babel/preset-env', {
                                targets: {
                                    chrome: 66,
                                    firefox: 60,
                                    edge: 42,
                                    safari: 12,
                                },
                                corejs: 3,
                                modules: false,
                                debug: false,
                                shippedProposals: true,
                            }],
                            ['@babel/preset-react', {
                                useBuiltIns: true,
                            }],
                            ['@babel/typescript', {
                                allExtensions: true,
                                isTSX: true,
                            }],
                        ],
                    },
                },
            },
            {
                type: 'javascript/auto',
                test: /\.json$/,
                include: [
                    path.resolve(__dirname, 'i18n'),
                ],
                exclude: [/en\.json$/],
                use: [
                    {
                        loader: 'file-loader?name=i18n/[name].[hash].[ext]',
                    },
                ],
            },
            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                    },
                    {
                        loader: 'sass-loader',
                        // options: {
                        //     includePaths: ['node_modules/compass-mixins/lib', 'sass'],
                        // },
                    },
                ],
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                    },
                ],
            },
        ],
    },
};
