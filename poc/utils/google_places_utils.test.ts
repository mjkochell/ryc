import testAddressData from '../testdata/address_info';

import {getCity, getPostalCode, getStateLong, getStateShort, getAddress, getCountryLong, getCountryShort, getFullAddress} from './google_places_utils';

describe('google_places_utils_test', () => {
    test('getCity', () => {
        const d = {address_components: testAddressData} as any;
        const city = getCity(d);
        if (!city) {
            expect(city).toBeTruthy();
            return;
        }

        expect(city).toEqual('Indianapolis');
    });

    test('getState', () => {
        const d = {address_components: testAddressData} as any;
        let state = getStateLong(d);
        if (!state) {
            expect(state).toBeTruthy();
            return;
        }

        expect(state).toEqual('Indiana');

        state = getStateShort(d);
        if (!state) {
            expect(state).toBeTruthy();
            return;
        }
        expect(state).toEqual('IN');
    });

    test('getCountry', () => {
        const d = {address_components: testAddressData} as any;
        let country = getCountryLong(d);
        if (!country) {
            expect(country).toBeTruthy();
            return;
        }

        expect(country).toEqual('United States');

        country = getCountryShort(d);
        if (!country) {
            expect(country).toBeTruthy();
            return;
        }
        expect(country).toEqual('US');
    });

    test('getPostalCode', () => {
        const d = {address_components: testAddressData} as any;
        const postalCode = getPostalCode(d);
        if (!postalCode) {
            expect(postalCode).toBeTruthy();
            return;
        }

        expect(postalCode).toEqual('46219');
    });

    test('getAddress', () => {
        const d = {address_components: testAddressData} as any;
        const address = getAddress(d);
        if (!address) {
            expect(address).toBeTruthy();
            return;
        }

        expect(address).toEqual('435 Shadeland Ave');
    });

    test('getFullAddress', () => {
        const d = {address_components: testAddressData} as any;
        const address = getFullAddress(d);
        if (!address) {
            expect(address).toBeTruthy();
            return;
        }

        expect(address).toEqual({
            country: 'US',
            state: 'IN',
            city: 'Indianapolis',
            postal_code: '46219',
            address: '435 Shadeland Ave',
        });
    });
});
