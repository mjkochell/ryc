export type PlacesService = google.maps.places.PlacesService;
export type PlaceResult = google.maps.places.PlaceResult;
export type GoogleMapType = google.maps.Map;

export type FullAddress = {
    country: string;
    state: string;
    city: string;
    postal_code: string;
    address: string;
}

function getFromAddressInfo(result: PlaceResult, key: string) {
    const addr = result.address_components;
    if (!addr) {
        return null;
    }

    for (const entry of addr) {
        if (entry.types.find((t) => t === key)) {
            return entry;
        }
    }

    return null;
}

export function getStateLong(result: PlaceResult) {
    const s = getFromAddressInfo(result, 'administrative_area_level_1');
    if (s) {
        return s.long_name;
    }

    return '';
}

export function getStateShort(result: PlaceResult) {
    const s = getFromAddressInfo(result, 'administrative_area_level_1');
    if (s) {
        return s.short_name;
    }

    return '';
}

export function getCity(result: PlaceResult) {
    const s = getFromAddressInfo(result, 'locality');
    if (s) {
        return s.long_name;
    }

    return '';
}

export function getPostalCode(result: PlaceResult) {
    const s = getFromAddressInfo(result, 'postal_code');
    if (s) {
        return s.long_name;
    }

    return '';
}

export function getCountryLong(result: PlaceResult) {
    const s = getFromAddressInfo(result, 'country');
    if (s) {
        return s.long_name;
    }

    return '';
}
export function getCountryShort(result: PlaceResult) {
    const s = getFromAddressInfo(result, 'country');
    if (s) {
        return s.short_name;
    }

    return '';
}

export function getAddress(result: PlaceResult) {
    let numberStr = '';
    const number = getFromAddressInfo(result, 'street_number');
    if (number) {
        numberStr = number.long_name;
    }

    let streetStr = '';
    const street = getFromAddressInfo(result, 'route');
    if (street) {
        streetStr = street.short_name;
    }

    return [numberStr, streetStr].filter(Boolean).join(' ');
}

export function getFullAddress(result: PlaceResult): FullAddress {
    return {
        country: getCountryShort(result),
        state: getStateShort(result),
        city: getCity(result),
        postal_code: getPostalCode(result),
        address: getAddress(result),
    };
}

export type Summary = FullAddress & {
    name: string;
    phone_number: string;
}

export function getSummary(result: PlaceResult): Summary {
    return {
        name: getName(result),
        phone_number: getPhoneNumber(result),
        ...getFullAddress(result),
    };
}

export function getName(result: PlaceResult): string {
    return result.name;
}

export function getPhoneNumber(result: PlaceResult): string {
    return result.international_phone_number || '';
}

export const createClient = (setService: (s: PlacesService) => void) => () => {
    setTimeout(() => {
        const serv = new window.google.maps.places.PlacesService(document.createElement('div'));
        setService(serv);
    }, 500);
};

export type PlaceDetailsRequest = google.maps.places.PlaceDetailsRequest;

export async function getPlaceDetails(service: PlacesService, cid: string): Promise<PlaceResult> {
    return new Promise((resolve, reject) => {
        const req: PlaceDetailsRequest = {
            placeId: cid,
        };

        service.getDetails(req, (result, status) => {
            if (status !== 'OK') {
                reject(new Error(`Google Places API returned status ${status}`));
                return;
            }
            resolve(result);
        });
    });
}

export function createMarker(map: GoogleMapType, result: PlaceResult) {
    const marker = new window.google.maps.Marker({
        map,
        place: {
            placeId: result.place_id,
            location: result.geometry.location,
        },
    });
    map.setCenter(result.geometry.location);
}
