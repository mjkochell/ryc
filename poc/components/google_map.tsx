import React from 'react';

const initialMapState = {
    center: {lat: -33.8666, lng: 151.1958},
    zoom: 15,
};

export default function GoogleMap({state, setState}) {

    React.useEffect(() => {
        setTimeout(() => {
            const m = new window.google.maps.Map(ref.current, initialMapState);
            const s = new window.google.maps.places.PlacesService(m);

            setState({map: m, service: s});
        }, 500);
    }, []);

    const ref = React.useRef(null);

    return (
        <div
            style={{
                width: '500px',
                height: '500px',
            }}
            ref={ref}
        />
    );
}
