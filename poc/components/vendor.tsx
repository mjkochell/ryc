import React from 'react';
import {useStoreState, State, useStoreActions, Actions} from 'easy-peasy';

import {StoreModel} from '../poc/store/store';
import {Purchase} from '../poc/store/purchase_store';

import PurchaseComponent from './purchase';

export default function VendorPage() {
    const vendor = useStoreState((state: State<StoreModel>) => state.vendors.items)[0];
    const purchases = useStoreState((state: State<StoreModel>) => state.purchases.getPurchasesForVendor(vendor));
    const {approvePurchase, declinePurchase} = useStoreActions((actions: Actions<StoreModel>) => actions.purchases);

    const renderApproval = (p: Purchase) => {
        if (p.approval !== 'needs_action') {
            return p.approval;
        }

        return (
            <div>
                <button onClick={() => approvePurchase(p)}>{'Approve'}</button>
                <button onClick={() => declinePurchase(p)}>{'Decline'}</button>
            </div>
        );
    };

    return (
        <div>
            {purchases.map((p) => (
                <div>
                    <PurchaseComponent
                        key={p.id}
                        purchase={p}
                    />
                    {renderApproval(p)}
                </div>
            ))}
        </div>
    );
}
