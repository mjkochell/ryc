import React from 'react';
import {useStoreState, State} from 'easy-peasy';

import ReactSelect from 'react-select';

import {StoreModel} from '../poc/store/store';
import {getFullAddress} from '../poc/utils/google_places_utils';
import {useForm} from '../poc/hooks/use-form';
import PurchaseComponent from './purchase';

export default function PatronPage() {
    const [{name, phone, email}, {setInputFieldValue}] = useForm({});

    const vendors = useStoreState((state: State<StoreModel>) => state.vendors.items);
    const charities = useStoreState((state: State<StoreModel>) => state.charities.items);
    const purchases = useStoreState((state: State<StoreModel>) => state.purchases.getPurchasesForPatron)({id: '1'});

    const vendorOptions = vendors.map((v) => ({
        label: `${v.name} - ${v.address}`,
        value: v.cid,
    }));

    const charityOptions = charities.map((c) => ({
        label: c.name,
        value: c.id,
    }));

    const fields = [
        {name: 'Name', value: name},
        {name: 'Phone', value: phone},
        {name: 'Email', value: email},
    ];

    return (
        <div>
            {fields.map((f) => (
                <div key={f.name}>
                    <label>{f.name}</label>
                    <div>
                        <input
                            name={f.name.toLowerCase()}
                            onChange={setInputFieldValue}
                            type={'text'}
                        />
                    </div>
                </div>
            ))}
            <input type={'file'}/>
            <p>
                {'Charity'}
            </p>
            <ReactSelect
                options={charityOptions}
            />
            <p>
                {'Restaurant'}
            </p>
            <ReactSelect
                options={vendorOptions}
            />
            <button>
                {'Submit'}
            </button>
            <div>
                <h2>{'Past Purchases'}</h2>
                {purchases.map((p) => (
                    <PurchaseComponent
                        purchase={p}
                        key={p.id}
                    />
                ))}
            </div>
        </div>
    );
}
