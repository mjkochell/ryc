import React from 'react';
import {State, useStoreState} from 'easy-peasy';

import {Purchase} from '../poc/store/purchase_store';
import {StoreModel} from '../poc/store/store';

type Props = {
    purchase: Purchase;
}

export default function PurchaseComponent({purchase}: Props) {
    const getPatron = useStoreState((state: State<StoreModel>) => state.patrons.getPatron);
    const patron = getPatron(purchase.patron_id);

    const renderApproval = () => {
        return <></>;
    };

    const text = `${patron.name} purchased something at . They also donated $3 to .`;

    return (
        <div>
            <div>{purchase.id}</div>
            {text}
            {purchase.receipt_image_url && (
                <img
                    src={purchase.receipt_image_url}
                    width={'500px'}
                    height={'700px'}
                />
            )}
            {renderApproval()}
        </div>
    );
}
