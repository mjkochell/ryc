import React from 'react';
import {useStoreState, State, Actions, useStoreActions} from 'easy-peasy';

import ReactSelect from 'react-select';

import {StoreModel, Vendor} from '../poc/store/store';
import {useForm} from '../poc/hooks/use-form';

import {PlacesService, PlaceResult, FullAddress, getFullAddress, createClient, getPlaceDetails, getSummary, Summary, createMarker, GoogleMapType} from '../poc/utils/google_places_utils';
import GoogleMap from './google_map';

const initialState = {
    vendorCID: 'ChIJVbnzXA5Pa4gR7r80xsuWpP8',
};

const initialService = null;

const initialVendorInfo: Summary = {
    name: '',
    phone_number: '',
    address: '',
    city: '',
    state: '',
    postal_code: '',
    country: '',
};

const initialCharityInfo = {
    name: '',
};

export default function AdminPage() {
    const [{vendorCID}, {setInputFieldValue}] = useForm(initialState);
    const [error, setError] = React.useState('');
    const [charityInfo, {setInputFieldValue: setCharityFieldValue}] = useForm(initialCharityInfo);
    const [vendorInfo, setVendorInfo] = React.useState<Summary>(initialVendorInfo);

    const vendors = useStoreState((state: State<StoreModel>) => state.vendors.items);
    const addVendor = useStoreActions((actions: Actions<StoreModel>) => actions.vendors.addVendor);

    const charities = useStoreState((state: State<StoreModel>) => state.charities.items);
    const createCharity = useStoreActions((actions: Actions<StoreModel>) => actions.charities.createCharity);

    const [{map, service}, setMapService] = React.useState<{map: GoogleMapType, service: PlacesService}>({map: null, service: null});

    const handleCharityCreate = () => {
        createCharity(charityInfo);
        setCharityFieldValue({target: {name: 'name', value: ''}});
    };

    const handleCIDSearch = async () => {
        const data = await getPlaceDetails(service, vendorCID);
        const info = getSummary(data);
        setVendorInfo(info);

        createMarker(map, data);
    };

    const vendorFields = [
        {name: 'Name', value: vendorInfo.name},
        {name: 'Phone Number', value: vendorInfo.phone_number},
        {name: 'Address', value: vendorInfo.address},
        {name: 'City', value: vendorInfo.city},
        {name: 'State', value: vendorInfo.state},
        {name: 'Postal Code', value: vendorInfo.postal_code},
        {name: 'Country', value: vendorInfo.country},
    ];

    const handleAddVendor = () => {
        const v: Vendor = {
            cid: vendorCID,
            ...vendorInfo,
        };

        addVendor(v);
    };

    const cidForm = (
        <div>
            <div>
                <label>{'Paste CID here'}</label>
            </div>
            <div>
                <input
                    value={vendorCID}
                    name={'vendorCID'}
                    onChange={setInputFieldValue}
                    type={'text'}
                />
                <button onClick={handleCIDSearch}>{'Submit'}</button>
            </div>
        </div>
    );

    const showFields = (ls: {name: string; value: string}[]) => ls.map((f) => (
        <div key={f.name}>
            <label>{f.name}</label>
            <div>
                <input
                    readOnly={true}
                    value={f.value}
                    name={f.name}
                    type={'text'}
                />
            </div>
        </div>
    ));

    const prettyVendorName = (v: Vendor) => `${v.name} - ${v.address}`;

    return (
        <div>
            <div>
                <h2>{'Manage Charities'}</h2>
                <ReactSelect
                    options={charities.map((c) => ({label: c.name, value: c.id}))}
                />
                <input
                    name={'name'}
                    value={charityInfo.name}
                    onChange={setCharityFieldValue}
                    type={'text'}
                />
                <button onClick={handleCharityCreate}>
                    {'Add Charity'}
                </button>
            </div>

            <hr/>

            <div>
                <h2>{'Manage Vendors'}</h2>
                <ReactSelect
                    options={vendors.map((v) => ({label: prettyVendorName(v), value: v.cid}))}
                />
                <div style={{height: '100px'}}/>
                {showFields(vendorFields)}
                <button onClick={handleAddVendor}>{'Add Vendor'}</button>
                <div style={{height: '40px'}}/>
                {cidForm}
            </div>

            {error}
            <GoogleMap
                state={{map, service}}
                setState={setMapService}
            />
        </div>
    );
}
