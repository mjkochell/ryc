import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect,
} from 'react-router-dom';

import useReactRouter from 'use-react-router';

import AdminPage from './admin';
import PatronPage from './poc/patron';
import VendorPage from './poc/vendor';
import CharityPage from './poc/charity';
import {saveState} from '../poc/store/helpers';

const Header = () => {
    const {history} = useReactRouter();
    return (
        <h1>{history.location.pathname.slice(1)}</h1>
    );
}

export default function App() {
    return (
        <Router>
            <button onClick={() => saveState({})}>
                {'Reset'}
            </button>
            <div>
                <Header/>
                <nav>
                    <ul>
                        <li>
                            <Link to='/admin'>{'Admin'}</Link>
                        </li>
                        <li>
                            <Link to='/patron'>{'Patron'}</Link>
                        </li>
                        <li>
                            <Link to='/vendor'>{'Vendor'}</Link>
                        </li>
                        <li>
                            <Link to='/charity'>{'Charity'}</Link>
                        </li>
                    </ul>
                </nav>

                <Switch>
                    <Route path='/admin'>
                        <AdminPage/>
                    </Route>
                    <Route path='/patron'>
                        <PatronPage/>
                    </Route>
                    <Route path='/vendor'>
                        <VendorPage/>
                    </Route>
                    <Route path='/charity'>
                        <CharityPage/>
                    </Route>
                    <Route
                        exact={true}
                        path='/'
                    >
                        <Redirect to='/admin'/>
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}
