import React from 'react';
import {useStoreState, State} from 'easy-peasy';

import {StoreModel} from '../poc/store/store';

export default function CharityPage() {
    const vendors = useStoreState((state: State<StoreModel>) => state.vendors.items);

    return (
        <div>
            {vendors.map((v) => (
                <h2 key={v.name}>
                    {v.name}
                </h2>
            ))}
        </div>
    );
}
