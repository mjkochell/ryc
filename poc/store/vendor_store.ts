import {Action} from 'easy-peasy';

import {addItem} from './common';

export type Vendor = {
    cid: string;
    name: string;
    address: string;
    // address2: string;
    city: string;
    state: string;
    postal_code: string;
};

export interface VendorStore {
    items: Vendor[];
    addVendor: Action<VendorStore, Vendor>;
}

export const vendorStore: VendorStore = {
    items: [],
    addVendor: addItem<Vendor>((v1, v2) => v1.cid === v2.cid),
};
