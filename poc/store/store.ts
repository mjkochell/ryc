import {createStore} from 'easy-peasy';

import {saveState, loadState} from './helpers';

import {MapStore, mapStore} from './map_store';
import {VendorStore, vendorStore} from './vendor_store';
import {CharityStore, charityStore} from './charity_store';
import {PatronStore, patronStore} from './patron_store';
import {PurchaseStore, purchaseStore} from './purchase_store';

export interface StoreModel {
    vendors: VendorStore;
    charities: CharityStore;
    patrons: PatronStore;
    maps: MapStore;
    purchases: PurchaseStore;
}

const model: StoreModel = {
    vendors: vendorStore,
    charities: charityStore,
    patrons: patronStore,
    maps: mapStore,
    purchases: purchaseStore,
};

let initialState;
const persisted = loadState();
if (persisted) {
    initialState = persisted;
}

const store = createStore(model, {
    initialState,
});

store.subscribe(() => {
    saveState(store.getState());
});

export default store;
