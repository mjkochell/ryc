import {action, computed} from 'easy-peasy';
import {T} from 'index';

type Equals<T> = (a: T, b: T) => boolean;
type GetID<T> = (a: T) => string;

export function addItem<T>(eq: Equals<T>) {
    return action((state: {items: T[]}, item: T) => {
        const index = state.items.findIndex((item2) => eq(item, item2));
        if (index === -1) {
            state.items.push(item);
            return;
        }

        state.items.splice(index, 1, item);
    });
}

export function getItem<T>(getID: GetID<T>) {
    return computed((state: {items: T[]}) => (id: string) => {
        return state.items.find((item) => getID(item) === id);
    });
}
