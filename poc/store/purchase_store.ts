import {Action, computed, Computed, action, thunk, Thunk} from 'easy-peasy';

import {addItem, getItem} from './common';
import {Patron} from './patron_store';
import {Vendor} from './vendor_store';
import {Charity} from './charity_store';

export type Purchase = {
    id: string;
    receipt_image_url: string;
    patron_id: string;
    vendor_id: string;
    charity_id: string;
    approval: string;
}

export interface PurchaseStore {
    items: Purchase[];
    addPurchase: Action<PurchaseStore, Purchase>;
    getPurchase: Computed<PurchaseStore, Purchase>;

    approvePurchase: Thunk<PurchaseStore, Purchase>;
    declinePurchase: Thunk<PurchaseStore, Purchase>;

    getPurchasesForPatron: Computed<PurchaseStore, (patron: Patron) => Purchase[]>;
    getPurchasesForVendor: Computed<PurchaseStore, (vendor: Vendor) => Purchase[]>;
    getPurchasesForCharity: Computed<PurchaseStore, (charity: Charity) => Purchase[]>;
}

export const purchaseStore: PurchaseStore = {
    items: [{
        id: '1',
        vendor_id: 'ChIJVbnzXA5Pa4gR7r80xsuWpP8',
        patron_id: '1',
        charity_id: '1',
        receipt_image_url: '',
        approval: 'needs_action',
    }],
    addPurchase: addItem<Purchase>((p1, p2) => p1.id === p2.id),
    getPurchase: getItem<Purchase>((p) => p.id),

    approvePurchase: thunk((actions, p) => {
        const approvedPurchase = {...p, approval: 'approved'};
        actions.addPurchase(approvedPurchase);
    }),
    declinePurchase: thunk((actions, p) => {
        const declinedPurchase = {...p, approval: 'declined'};
        actions.addPurchase(declinedPurchase);
    }),

    getPurchasesForPatron: computed((state) => (patron) => {
        return state.items.filter((purchase) => purchase.patron_id === patron.id);
    }),

    getPurchasesForVendor: computed((state) => (vendor) => {
        return state.items.filter((purchase) => purchase.vendor_id === vendor.cid);
    }),

    getPurchasesForCharity: computed((state) => (charity) => {
        return state.items.filter((purchase) => purchase.charity_id === charity.id);
    }),
};
