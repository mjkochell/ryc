import {Action, action} from 'easy-peasy';

export type MapStore = {
    map: any;
    service: any;
    setMap: Action<MapStore, any>;
    setService: Action<MapStore, any>;
}

export const mapStore: MapStore = {
    map: null,
    service: null,
    setMap: action((state, map) => {
        state.map = map;
    }),
    setService: action((state, service) => {
        state.service = service;
    }),
};
