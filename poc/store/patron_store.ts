import {Action, Computed} from 'easy-peasy';

import {addItem, getItem} from './common';

export type Patron = {
    id: string;
    name: string;
}

export interface PatronStore {
    items: Patron[];
    addPatron: Action<PatronStore, Patron>;
    getPatron: Computed<PatronStore, (id: string) => Patron>;
}

export const patronStore: PatronStore = {
    items: [{
        id: '1',
        name: 'Michael',
    }],
    addPatron: addItem<Patron>((p1, p2) => p1.id === p2.id),
    getPatron: getItem<Patron>((p) => p.id),
};
