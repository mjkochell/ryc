import {Action, action, thunk, Thunk} from 'easy-peasy';

import {addItem} from './common';

export type Charity = {
    id: string;
    name: string;
}

export interface CharityStore {
    items: Charity[];
    addCharity: Action<CharityStore, Charity>;
    createCharity: Thunk<CharityStore, Partial<Charity>>;
}

export const charityStore: CharityStore = {
    items: [],
    addCharity: addItem<Charity>((v1, v2) => v1.id === v2.id),
    createCharity: thunk((actions, charity, {getState}) => {
        const id = getState().items.length + 1;
        const newCharity: Charity = {...charity, id: id.toString()};
        actions.addCharity(newCharity);
    }),
};
