export default [
    {
        long_name: '435',
        short_name: '435',
        types: ['street_number'],
    },
    {
        long_name: 'Shadeland Avenue',
        short_name: 'Shadeland Ave',
        types: ['route'],
    },
    {
        long_name: 'Eastgate',
        short_name: 'Eastgate',
        types: ['neighborhood', 'political'],
    },
    {
        long_name: 'Indianapolis',
        short_name: 'Indianapolis',
        types: ['locality', 'political'],
    },
    {
        long_name: 'Warren Township',
        short_name: 'Warren Township',
        types: ['administrative_area_level_3', 'political'],
    },
    {
        long_name: 'Marion County',
        short_name: 'Marion County',
        types: ['administrative_area_level_2', 'political'],
    },
    {
        long_name: 'Indiana',
        short_name: 'IN',
        types: ['administrative_area_level_1', 'political'],
    },
    {
        long_name: 'United States',
        short_name: 'US',
        types: ['country', 'political'],
    },
    {
        long_name: '46219',
        short_name: '46219',
        types: ['postal_code'],
    },
];
