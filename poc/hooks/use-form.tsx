import React, {useState} from 'react';

export type FormActions = {
    setValueByInputName: (e: React.ChangeEvent<HTMLInputElement>) => void;
    setValueByPropertyName: (name: string, value: string) => void;
}

export function useForm<T>(initialState: T): [T, FormActions] {
    const [state, setState] = useState(initialState);

    const actions: FormActions = {
        setValueByInputName: (e) => {
            const {target: {name, value}} = e;

            setState({
                ...state,
                [name]: value,
            });
        },
        setValueByPropertyName: (name, value) => {
            setState({
                ...state,
                [name]: value,
            });
        }
    };
    return [state, actions];
}
