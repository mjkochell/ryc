import {Thunk, thunk} from 'easy-peasy';

// import csvData from '../data/merchants_and_charities_large_file';
import {merchantCodes, charityCodes} from '../data/old/sic_codes';

import {StoreModel} from './store';
import {SourceEntity} from './entities';

export interface InitStore {
    init: Thunk<InitStore, void, void, StoreModel>;
}

export const initStore: InitStore = {
    init: thunk((_1, _2, {dispatch}) => {
        const split = csvData.split('\n');
        const topRow = split[0].split('\t');
        const rows = split.slice(1);

        const entities = rows.map((row: string) => {
            const m = {} as any;
            const values = row.split('\t');
            topRow.forEach((col, i) => {
                m[col] = values[i];
            });

            return m;
        }) as SourceEntity[];

        const merchants = entities.filter((entity: SourceEntity) => {
            const sic = entity['Primary SIC Code'].split('-')[0];
            return merchantCodes.includes(sic);
        });

        const charities = entities.filter((entity: SourceEntity) => {
            const sic = entity['Primary SIC Code'].split('-')[0];
            return charityCodes.includes(sic);
        });

        charities.forEach((c) => {
            c.logo = 'https://irp-cdn.multiscreensite.com/41fd1102/dms3rep/multi/mobile/RCFLogo209x209.png';
            c.videoURL = 'https://www.youtube.com/watch?v=G5r838zmuK4';
            c.website = 'http://www.rileykids.org';
        });

        dispatch.merchants.addItems(merchants);
        dispatch.charities.addItems(charities);
    }),
};
