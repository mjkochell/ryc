import {createStore} from 'easy-peasy';

// import {saveState, loadState} from './helpers';

import {MerchantStore, merchantStore} from './merchant_store';
import {CharityStore, charityStore} from './charity_store';
import {InitStore, initStore} from './init_store';

export interface StoreModel {
    merchants: MerchantStore;
    charities: CharityStore;
    init: InitStore;
}

const model: StoreModel = {
    merchants: merchantStore,
    charities: charityStore,
    init: initStore,
};

let initialState = {};
// const persisted = loadState();
// if (persisted) {
//     initialState = persisted;
// }

const store = createStore(model, {
    initialState,
});

// store.subscribe(() => {
//     const state = store.getState();
//     saveState({});
// });

export default store;
