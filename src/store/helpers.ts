import {State} from 'easy-peasy';

import {StoreModel} from './store';

export const loadState = () => {
    const state = localStorage.getItem('redux-state');
    if (!state) {
        return null;
    }
    return JSON.parse(state);
};

export const saveState = (state: State<StoreModel>) => {
    if (!state) {
        return;
    }
    const toPersist = state;
    localStorage.setItem('redux-state', JSON.stringify(toPersist));
};
