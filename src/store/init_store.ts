import {Thunk, thunk} from 'easy-peasy';

import {getLocalCharities, getLocalMerchants, getRemoteCharities, getRemoteMerchants} from '../get-entities';

import {StoreModel} from './store';
import {ping} from 'client/client';

export interface InitStore {
    init: Thunk<InitStore, void, void, StoreModel>;
}

export const initStore: InitStore = {
    init: thunk((x, y, {dispatch}) => {
        ping();
        const placeholder = document.getElementById('placeholder');
        if (placeholder) {
            placeholder.style.display = 'none';
        }

        dispatch.charities.addItems(getLocalCharities());
        dispatch.merchants.addItems(getLocalMerchants());

        getRemoteCharities().then(data => dispatch.charities.addItems(data));
        getRemoteMerchants().then(data => dispatch.merchants.addItems(data));
    }),
};
