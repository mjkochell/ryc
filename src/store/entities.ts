import {CSVOutputEntity} from "data/export-format/interfaces";

export interface SourceEntity {
    'IUSA Number': string;
    'Primary SIC Code': string;
    'Company Name': string;
    'Location Address': string;
    'Location City': string;
    'Location State': string;
    'Location Zip': string;
    'Phone Number Combined': string;
}

export type Entity = {
    type: string; // Merchant, Charity, CharityGroup
    id: string;
    name: string;
    logo?: string;
    videoURL?: string;
    website?: string;
    address?: string;
    parent_id?: string;
};

export type WithInfo = {
    logo?: string;
    videoURL?: string;
    website?: string;
};

export type CharityGroup = {
    id: string;
    parent_id: string;
    location_id: string;
    name: string;
};

export type CharityLocation = {
    id: string;
    parent_id: string;
    name: string;
    address: string;
    groups: CharityGroup[];
    csvData: CSVOutputEntity;
} & WithInfo;

export type Charity = {
    id: string;
    name: string;
    locations: CharityLocation[];
} & WithInfo;

export type MerchantLocation = {
    id: string;
    parent_id: string;
    name: string;
    address: string;
    csvData: CSVOutputEntity;
} & WithInfo;

export type Merchant = {
    id: string;
    name: string;
    locations: MerchantLocation[];
} & WithInfo;
