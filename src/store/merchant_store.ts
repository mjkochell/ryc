import {Action, action, computed, memo, Computed} from 'easy-peasy';

import {MerchantFormState} from 'components/merchant_form';

import {Merchant, MerchantLocation} from './entities';

const MEMO_CACHE_SIZE = 100;

export interface MerchantStore {
    items: Merchant[];
    addItems: Action<MerchantStore, Merchant[]>;
    parentMerchants: Computed<MerchantStore, Merchant[]>;
    getLocationsByParentID: Computed<MerchantStore, (parentID: string) => MerchantLocation[]>;
    getParentIDFromLocationID: Computed<MerchantStore, (locationID: string) => string>;
    getFullForm: Computed<MerchantStore, (form: MerchantFormState) => Merchant>;
}

export const merchantStore: MerchantStore = {
    items: [],
    addItems: action((state, items) => {
        // Array.prototype.push.apply(state.items, items);
        state.items = items;
    }),
    getLocationsByParentID: computed((state) => memo(
        (parentID: string) => {
            const parent = state.items.find((e) => e.id === parentID);
            if (parent) {
                return parent.locations || [];
            }

            return [];
        },
        MEMO_CACHE_SIZE
    )),
    parentMerchants: computed(({items}) => Array.from(new Set(items.filter((i) => !i.parent_id)))),
    getParentIDFromLocationID: computed(({items}) => (locID: string) => {
        const parent = items.find(m => m.locations.find(loc => loc.id === locID));
        if (!parent) {
            return '';
        }

        return parent.id;
    }),
    getFullForm: computed((state) => (form: MerchantFormState) => {
        const parent = state.items.find((e) => e.id === form.merchant);
        if (parent && parent.locations) {
            const loc = parent.locations.find((l) => l.id === form.merchantLocation);
            if (loc) {
                return {...parent, locations: [loc]};
            }
        }

        return {} as Merchant;
    }),
};
