import {Action, action, computed, memo, Computed} from 'easy-peasy';

import {Charity, CharityLocation, CharityGroup} from './entities';
import {parseAsync} from '@babel/core';
import {CharityFormState} from 'components/charity_form';

const MEMO_CACHE_SIZE = 100;

export interface CharityStore {
    items: Charity[];
    addItems: Action<CharityStore, Charity[]>;
    parentCharities: Computed<CharityStore, Charity[]>;
    getLocationsByParentID: Computed<CharityStore, (parentID: string) => CharityLocation[]>;
    getParentIDFromLocationID: Computed<CharityStore, (locationID: string) => string>;
    getGroupsByParentIDAndLocationID: Computed<CharityStore, (parentID: string, locationID: string) => CharityGroup[]>;
    getFullForm: Computed<CharityStore, (form: CharityFormState) => Charity>;
}

export const charityStore: CharityStore = {
    items: [],
    addItems: action((state, items) => {
        // Array.prototype.push.apply(state.items, items);
        state.items = items;
    }),
    getLocationsByParentID: computed((state) => memo(
        (parentID: string) => {
            const parent = state.items.find((e) => e.name === parentID);
            if (parent) {
                return parent.locations || [];
            }

            return [];
        },
        MEMO_CACHE_SIZE
    )),
    getGroupsByParentIDAndLocationID: computed((state) => memo(
        (parentID: string, locationID: string) => {
            const parent = state.items.find((e) => e.id === parentID);
            if (parent && parent.locations) {
                const loc = parent.locations.find((l) => l.id === locationID);
                if (loc) {
                    return loc.groups || [];
                }
            }

            return [];
        },
        MEMO_CACHE_SIZE
    )),
    getParentIDFromLocationID: computed(({items}) => (locID: string) => {
        const parent = items.find(m => m.locations.find(loc => loc.id === locID));
        if (!parent) {
            return '';
        }

        return parent.id;
    }),
    parentCharities: computed(({items}) => Array.from(new Set(items.filter((i) => !i.parent_id)))),
    getFullForm: computed((state) => (form: CharityFormState) => {
        const parent = state.items.find((e) => e.id === form.charity);
        if (parent && parent.locations) {
            const loc = parent.locations.find((l) => l.id === form.charityLocation);
            if (loc) {
                return {...parent, locations: [loc]};
            }
        }

        return {} as Charity;
    }),
};
