import React from 'react';
import {Button} from '@material-ui/core';

import styles from '../styles/app.scss';

type URLResponse = {
    download_url: string;
    upload_url: string;
}

const getUploadURL = (file: File): Promise<URLResponse> => {
    const url = `${process.env.API_GATEWAY_URL}/url?fname=${file.name}`;
    return fetch(url).then((r) => r.json());
}

export default function FileUpload({setUploadedURL}) {
    const [url, setURL] = React.useState('');
    const setFile = async (e: any) => {
        const files: FileList = e.target.files;
        const file = files[0];
        const res = await getUploadURL(file);
        fetch(res.upload_url, {
            method: 'PUT',
            body: file,
            headers: {
                // 'random-he'
                // 'access-control-request-headers': 'haha',
                // 'access-control-allow-headers': 'content-type',
                // 'Content-Type': '',
            },
        }).then(() => {
            const imageURL = res.download_url;
            setURL(imageURL);
            setUploadedURL(imageURL);
        })
    };

    return (
        <div>
            <Button
                onClick={() => document.getElementById('ryc-file-upload').click()}
                className={'ryc-button'}
                variant={'contained'}
                color={'primary'}
            >
                {'Upload Receipt'}
            </Button>
            <input id='ryc-file-upload' type='file' onChange={setFile} style={{display: 'none'}} accept={'image/png,image/jpeg'}/>
            {process.env.QA === 'true' && <a href={url}>{url}</a>}
            {url && (
                <span
                    style={{marginLeft: '10px'}}
                    className={'emoji'}
                    role={'img'}
                    // aria-label={props.label ? props.label : ''}
                    // aria-hidden={props.label ? 'false' : 'true'}
                >
                    {'✔️ Uploaded'}
                </span>
            )}
        </div>
    );
}
