import React from 'react';

import ReactSelect from 'react-select';
import {useStoreState, State} from 'easy-peasy';

import {StoreModel} from 'store/store';

import {SourceEntity, Entity, CharityLocation} from 'store/entities';

import LimitedSelect from './select';

export type CharityFormState = {
    charity: string;
    setCharity: (charityID: string) => void;
    charityLocation: string;
    setCharityLocation: (locationID: string) => void;
    charityGroup: string;
    setCharityGroup: (groupID: string) => void;
}

type Props = {
    charityForm: CharityFormState;
}

export default function CharityForm(props: Props) {
    const {charityForm} = props;

    const parentCharities = useStoreState((state: State<StoreModel>) => state.charities.parentCharities);
    const getCharityLocations = useStoreState((state: State<StoreModel>) => state.charities.getLocationsByParentID);
    const getCharityGroups = useStoreState((state: State<StoreModel>) => state.charities.getGroupsByParentIDAndLocationID);

    const setParentCharity = (value: string) => {
        charityForm.setCharity(value);
        const locs = getCharityLocations(value);
        if (locs.length === 1) {
            selectCharityLocation(locs[0].id);
        } else {
            selectCharityLocation('');
        }
    };

    const selectCharityLocation = (value: string) => {
        charityForm.setCharityLocation(value);
        charityForm.setCharityGroup('');
    };

    const selectCharityGroup = (value: string) => {
        charityForm.setCharityGroup(value);
    };

    const charityOptions = parentCharities.map((charity) => ({
        label: charity.name,
        value: charity.id,
    }));

    let charitySelect = <ReactSelect/>;
    if (parentCharities.length) {
        charitySelect = (
            <LimitedSelect
                value={charityOptions.find(({value}) => value === charityForm.charity)}
                options={charityOptions}
                onChange={setParentCharity}
            />
        );
    }

    let charityLocationSelect = <ReactSelect/>;
    if (charityForm.charity) {
        const charityLocations = getCharityLocations(charityForm.charity);
        const locationOptions = charityLocations.map((loc) => ({
            label: loc.name,
            value: loc.id,
        }));

        charityLocationSelect = (
            <ReactSelect
                value={locationOptions.find((loc) => loc.value === charityForm.charityLocation)}
                options={locationOptions}
                onChange={({value}) => selectCharityLocation(value)}
            />
        );
    }

    let charityGroupSelect = <ReactSelect/>;
    if (charityForm.charity && charityForm.charityLocation) {
        const charityGroups = getCharityGroups(charityForm.charity, charityForm.charityLocation);
        const groupOptions = charityGroups.map((group) => ({
            label: group.name,
            value: group.id,
        }));

        charityGroupSelect = (
            <ReactSelect
                value={groupOptions.find((group) => group.value === charityForm.charityGroup)}
                options={groupOptions}
                onChange={({value}) => selectCharityGroup(value)}
            />
        );
    }

    return (
        <div>
            <h3>{'Charity'}</h3>
            <div>
                <label>{'Charity Name'}</label>
                {charitySelect}
            </div>
            <div>
                <label>{'Charity Location'}</label>
                {charityLocationSelect}
            </div>
            <div>
                <label>{'Charity Group'}</label>
                {charityGroupSelect}
            </div>
        </div>
    );
}
