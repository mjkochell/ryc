import React from 'react';
import {useStoreState, State} from 'easy-peasy';

import {StoreModel} from 'store/store';
import {Charity} from 'store/entities';

import CharityShowcase from './charity_showcase';
import CharityForm, {CharityFormState} from './charity_form_extra';
import {getFormContentInput, getGlobalForm} from 'util/dom';

const expandForm = (form: CharityFormState, charities: Charity[]) => {
    const result = {...form};

    const charity = charities.find((c) => c.id === form.charity);
    if (!charity) {
        return result;
    }

    const {locations, ...charityExpanded} = charity;
    result.charityExpanded = charityExpanded;

    if (form.charityLocation) {

        const location = locations.find((loc) => loc.id === form.charityLocation);
        if (location) {
            const {groups, ...locationExpanded} = location;
            result.locationExpanded = locationExpanded;

            if (form.charityGroup) {
                const group = groups.find((g) => g.id === form.charityGroup);
                result.groupExpanded = group;
            }
        }
    }

    return result;
};

export default function WebsiteFormExtra() {
    const [selectedCharity, setSelectedCharity] = React.useState<string>('');
    const [selectedCharityLocation, setSelectedCharityLocation] = React.useState<string>('');
    const [selectedCharityGroup, setSelectedCharityGroup] = React.useState<string>('');

    const parentCharities = useStoreState((state: State<StoreModel>) => state.charities.parentCharities);

    const charityForm = {
        charity: selectedCharity,
        setCharity: setSelectedCharity,
        charityLocation: selectedCharityLocation,
        setCharityLocation: setSelectedCharityLocation,
        charityGroup: selectedCharityGroup,
        setCharityGroup: setSelectedCharityGroup,
    };

    const submitForm = (e) => {
        const input = getFormContentInput();
        input.value = JSON.stringify(expandForm(charityForm, parentCharities), null, 2);

        const form = getGlobalForm();
        form.submit.click();
    };

    return (
        <div
            className={'container-fluid'}
            style={styles.container}
        >
            <div className={'row'}>
                <div
                    className={'col-md-6'}
                    style={styles.half}
                >
                    <div style={styles.innerHalf}>
                        <CharityForm charityForm={charityForm}/>
                        <div>
                            <input type='file'/>
                        </div>
                    </div>
                    <button onClick={submitForm}>{'Submit'}</button>
                </div>
                <div
                    className={'col-md-6'}
                    style={styles.half}
                >
                    <div style={styles.innerHalf}>
                        <CharityShowcase charityForm={charityForm}/>
                    </div>
                </div>
            </div>
        </div>
    );
}

const styles = {
    container: {
        paddingLeft: '20px',
        // marginTop: '100px',
        width: '100%',

        // borderStyle: 'solid',
        position: 'relative' as 'relative',
    },
    half: {

        // display: 'inline-block',

        // paddingLeft: '20px',
        // marginTop: '100px',
        // width: '40%',
        // height: '500px',
        // borderStyle: 'solid',
    },
    innerHalf: {

        // position: 'absolute',
        // top: 0,
        width: '100%',
    },
    img: {
        position: 'absolute' as 'absolute',
        top: 0,
    },
};
