import React from 'react';

import {useStoreState, State} from 'easy-peasy';

import {getUrlParams} from 'utils';

import {StoreModel} from 'store/store';

import {CharityFormState} from './charity_form';
import {MerchantFormState} from './merchant_form';

type Props = {
    charityForm: CharityFormState;
    merchantForm: MerchantFormState;
};

export default function PopulateFromQRCode({charityForm, merchantForm}: Props) {
    const getMerchantParentID = useStoreState((state: State<StoreModel>) => state.merchants.getParentIDFromLocationID);
    const getCharityParentID = useStoreState((state: State<StoreModel>) => state.charities.getParentIDFromLocationID);

    React.useEffect(() => {
        const params = getUrlParams(window.location.search) as {merchant_id?: string; charity_id?: string};
        if (params.merchant_id) {
            const parentID = getMerchantParentID(params.merchant_id);
            if (parentID) {
                merchantForm.setMerchant(parentID);
                merchantForm.setMerchantLocation(params.merchant_id);
                merchantForm.setFromQRCode(true);
            }
        }
        if (params.charity_id) {
            const parentID = getCharityParentID(params.charity_id);
            if (parentID) {
                charityForm.setCharity(parentID);
                charityForm.setCharityLocation(params.charity_id);
                charityForm.setFromQRCode(true);
            }
        }
    }, []);
    return null;
}
