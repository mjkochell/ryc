import React from 'react';
import {useStoreState, State} from 'easy-peasy';

import {StoreModel} from 'store/store';

import Select from './select_mui';

export type MerchantFormState = {
    merchant: string;
    setMerchant: (merchantID: string) => void;
    merchantText: string;
    setMerchantText: (merchantID: string) => void;
    merchantLocation: string;
    setMerchantLocation: (locationID: string) => void;
    merchantLocationText: string;
    setMerchantLocationText: (locationID: string) => void;
    fromQRCode: boolean;
    setFromQRCode: (from: boolean) => void;
}

type Props = {
    merchantForm: MerchantFormState;
}

export default function MerchantForm(props: Props) {
    const {merchantForm} = props;

    const parentMerchants = useStoreState((state: State<StoreModel>) => state.merchants.items);
    const getMerchantLocations = useStoreState((state: State<StoreModel>) => state.merchants.getLocationsByParentID);

    const setParentMerchant = (value: string) => {
        merchantForm.setMerchant(value);
        const locs = getMerchantLocations(value);
        if (locs.length === 1) {
            selectMerchantLocation(locs[0].id);
        } else {
            selectMerchantLocation('');
        }
    };

    const selectMerchantLocation = (value: string) => {
        merchantForm.setMerchantLocation(value);
    };

    const merchantOptions = parentMerchants.map((merchant) => ({
        label: merchant.name,
        value: merchant.id,
    }));

    const merchantProps = {label: 'Merchant', inputValue: merchantForm.merchantText, onInputChange: merchantForm.setMerchantText}
    let merchantSelect = <Select {...merchantProps}/>;
    if (parentMerchants.length) {
        merchantSelect = (
            <Select
                {...merchantProps}
                value={merchantForm.merchant}
                options={merchantOptions}
                onChange={setParentMerchant}
                label={'Merchant'}
                disabled={Boolean(merchantForm.fromQRCode)}
            />
        );
    }
    const merchantLocationProps = {label: 'Merchant Location', inputValue: merchantForm.merchantLocationText, onInputChange: merchantForm.setMerchantLocationText}
    let merchantLocationSelect = <Select {...merchantLocationProps}/>;
    if (merchantForm.merchant) {
        const merchantLocations = getMerchantLocations(merchantForm.merchant);
        const locationOptions = merchantLocations.map((loc) => ({
            label: loc.address,
            value: loc.id,
        }));

        merchantLocationSelect = (
            <Select
                {...merchantLocationProps}
                value={merchantForm.merchantLocation}
                options={locationOptions}
                onChange={(value) => selectMerchantLocation(value)}
                disabled={merchantForm.fromQRCode}
            />
        );
    }

    return (
        <div>
            <div>
                {merchantSelect}
            </div>
            <div>
                {merchantLocationSelect}
            </div>
        </div>
    );
}
