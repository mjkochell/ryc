import React from 'react';

import ReactSelect from 'react-select';
import AsyncSelect from 'react-select/async';

const MAX_NUM_OPTIONS = 100;

type ReactSelectOption = {
    label: string;
    value: string;
}

export type Props = {
    options: ReactSelectOption[];
    onChange: (value: string) => void;
}

export default function LimitedSelect(props: Props) {
    if (!props.options.length) {
        return <ReactSelect/>;
    }
    const handleChange = (value: ReactSelectOption) => {
        if (props.onChange) {
            props.onChange(value.value);
        }
    };

    // Standard search term matching plus reducing to < 100 items
    const filterOptions = (input: string) => {
        let options = props.options;
        if (input) {
            options = options.filter((x) => x.label.toUpperCase().includes(input.toUpperCase()));
        }
        return Promise.resolve(options.slice(0, MAX_NUM_OPTIONS));
    };

    const selectComponent = (
        <AsyncSelect
            {...props}
            loadOptions={filterOptions}
            defaultOptions={true}
            menuPortalTarget={document.body}
            menuPlacement='auto'
            onChange={handleChange}
            // styles={getStyleForReactSelect(this.props.theme)}
        />
    );

    return selectComponent;
}
