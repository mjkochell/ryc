import React from 'react';
import { useStoreState, State, useStoreActions, Actions } from 'easy-peasy';

import { Button, TextField } from '@material-ui/core';

import { StoreModel } from 'store/store';
import { Charity } from 'store/entities';

import { submitForm as submit } from '../client/client';

import { getUrlParams } from 'utils';

import CharityForm, { CharityFormState } from './charity_form';
import FileUpload from './file_upload';
import MerchantForm, { MerchantFormState } from './merchant_form';
import PopulateFromQRCode from './populate-from-qr-code';
import {useForm} from 'hooks/use-form';

const useCharityForm = (): CharityFormState => {
    const [state, actions] = useForm({
        charity: '',
        charityText: '',
        charityLocation: '',
        charityLocationText: '',
        charityGroup: '',
    })
    const [fromQRCode, setFromQRCode] = React.useState<boolean>(false);
    const [selectedPrimarySIC, setPrimarySIC] = React.useState('');
    const [selectedSIC1, setSIC1] = React.useState('');

    return {
        charity: state.charity,
        setCharity: (value) => actions.setValueByPropertyName('charity', value),
        charityText: state.charityText,
        setCharityText: (value) => actions.setValueByPropertyName('charityText', value),
        charityLocation: state.charityLocation,
        setCharityLocation: (value) => actions.setValueByPropertyName('charityLocation', value),
        charityLocationText: state.charityLocationText,
        setCharityLocationText: (value) => actions.setValueByPropertyName('charityLocationText', value),
        charityGroup: state.charityGroup,
        setCharityGroup: (value) => actions.setValueByPropertyName('charityGroup', value),
        fromQRCode,
        setFromQRCode,
        selectedPrimarySIC,
        setPrimarySIC,
        selectedSIC1,
        setSIC1,
    };
};

const useMerchantForm = (): MerchantFormState => {
    const [state, actions] = useForm({
        merchant: '',
        merchantText: '',
        merchantLocation: '',
        merchantLocationText: '',
    });
    const [fromQRCode, setFromQRCode] = React.useState<boolean>(false);

    return {
        merchant: state.merchant,
        setMerchant: (value) => actions.setValueByPropertyName('merchant', value),
        merchantText: state.merchantText,
        setMerchantText: (value) => actions.setValueByPropertyName('merchantText', value),
        merchantLocation: state.merchantLocation,
        setMerchantLocation: (value) => actions.setValueByPropertyName('merchantLocation', value),
        merchantLocationText: state.merchantLocationText,
        setMerchantLocationText: (value) => actions.setValueByPropertyName('merchantLocationText', value),
        fromQRCode,
        setFromQRCode,
    };
};

const emailIsValid = (email: string) => {
    return Boolean(email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/));
};

const phoneNumberIsValid = (phone: string) => {
  const toMatch = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  return Boolean(phone.match(toMatch));
}

export default function WebsiteForm() {
    const [uploadedURL, setUploadedURL] = React.useState<string>('');
    const [message, setMessage] = React.useState<string>('');
    const [email, setEmail] = React.useState<string>('');
    const [phone, setPhone] = React.useState<string>('');
    const [name, setName] = React.useState<string>('');

    const charityForm = useCharityForm();
    const merchantForm = useMerchantForm();
    React.useEffect(() => {
        setMessage('');
    }, [charityForm.charityLocation, merchantForm.merchantLocation, uploadedURL]);

    const getFullCharityForm = useStoreState((state: State<StoreModel>) => state.charities.getFullForm);
    const getFullMerchantForm = useStoreState((state: State<StoreModel>) => state.merchants.getFullForm);
    const merchants = useStoreState((state: State<StoreModel>) => state.merchants.items);
    const charities = useStoreState((state: State<StoreModel>) => state.charities.items);

    const submitForm = async (e) => {
        const form = {
            ...charityForm,
            ...merchantForm,
            receipt_url: uploadedURL,
            name,
            email,
            phone,
        };
        if (
            !((charityForm.charity || charityForm.charityText) && (charityForm.charityLocation || charityForm.charityLocationText) &&
            (merchantForm.merchant || merchantForm.merchantText) && (merchantForm.merchantLocation || merchantForm.merchantLocationText))
        ) {
            setMessage('Please fill out the form.');
            return;
        }
        if (
            !form.receipt_url
        ) {
            setMessage('Please upload your receipt.');
            return;
        }

        if (!form.email && !form.phone) {
            setMessage('Please enter an email address or a phone number.');
            return;
        }

        if (form.phone && !phoneNumberIsValid(form.phone)) {
            setMessage('Please enter a valid phone number.');
            return;
        }

        if (form.email && !emailIsValid(form.email)) {
            setMessage('Please enter a valid email address.');
            return;
        }

        const charityFullForm = getFullCharityForm(charityForm);
        const merchantFullForm = getFullMerchantForm(merchantForm);

        const charityLocation = charityFullForm.locations ? charityFullForm.locations[0].address : '';
        const merchantLocation = merchantFullForm.locations ? merchantFullForm.locations[0].address : '';

        const body = {
            charity_name: charityForm.charity,
            charity_text: charityForm.charityText,
            charity_id: charityForm.charityLocation,
            charity_location: charityLocation,
            charity_location_text: charityForm.charityLocationText,
            charity_group: charityForm.charityGroup,
            merchant_name: merchantForm.merchant,
            merchant_text: merchantForm.merchantText,
            merchant_id: merchantForm.merchantLocation,
            merchant_location: merchantLocation,
            merchant_location_text: merchantForm.merchantLocationText,
            receipt_url: uploadedURL,
            name,
            email,
            phone,
        };

        try {
            const res = await submit(body);
            setMessage('✔️ Form Submitted');
        } catch (e) {
            setMessage('Error submitting');
        }
    };

    return (
        <div
            className={'container-fluid'}
            style={styles.container}
        >
            {merchants.length && charities.length ? <PopulateFromQRCode charityForm={charityForm} merchantForm={merchantForm} /> : null}
            <div className={'row'}>
                <div
                    className={'col-md-6 col-xs-12'}
                    style={styles.half}
                >
                    <div style={styles.innerHalf}>
                        <div className={'ryc-form-group'}>
                            <CharityForm charityForm={charityForm} />
                        </div>
                        <div className={'ryc-form-group'}>
                            <MerchantForm merchantForm={merchantForm} />
                        </div>
                        <div className={'ryc-form-group'}>
                            <div>
                                <TextField
                                    // {...params}
                                    onChange={e => setName(e.target.value)}
                                    value={name}
                                    type={'text'}
                                    style={{ width: '100%' }}
                                    label={'Name (optional)'}
                                    variant={'outlined'}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </div>
                            <div>
                                <TextField
                                    // {...params}
                                    onChange={e => setEmail(e.target.value)}
                                    value={email}
                                    type={'email'}
                                    style={{ width: '100%' }}
                                    type={'Email'}
                                    label={'Email'}
                                    variant={'outlined'}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </div>
                            <div>
                                <TextField
                                    // {...params}
                                    onChange={e => setPhone(e.target.value)}
                                    value={phone}
                                    type={'phone'}
                                    style={{ width: '100%' }}
                                    label={'or Phone'}
                                    variant={'outlined'}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </div>
                        </div>
                        <div className={'ryc-form-group'}>
                            <FileUpload setUploadedURL={setUploadedURL} />
                        </div>
                        <div className={'ryc-form-group'}>
                            <div>
                                <Button
                                    onClick={submitForm}
                                    className={'ryc-button'}
                                    variant={'contained'}
                                    color={'primary'}
                                >
                                    {'Submit'}
                                </Button>
                            </div>
                        </div>
                        {message && (
                            <span
                                style={{ marginLeft: '10px' }}
                                className={'emoji'}
                                role={'img'}
                            // aria-label={props.label ? props.label : ''}
                            // aria-hidden={props.label ? 'false' : 'true'}
                            >
                                {message}
                            </span>
                        )}
                    </div>
                </div>
            </div>
        </div>
    );
}

const styles = {
    container: {
        paddingLeft: '20px',
        // marginTop: '100px',
        width: '100%',

        // borderStyle: 'solid',
        position: 'relative' as 'relative',
    },
    half: {
        padding: 0,

        // display: 'inline-block',

        // paddingLeft: '20px',
        // marginTop: '100px',
        // width: '40%',
        // height: '500px',
        // borderStyle: 'solid',
    },
    innerHalf: {

        // position: 'absolute',
        // top: 0,
        width: '100%',
    },
    img: {
        position: 'absolute' as 'absolute',
        top: 0,
    },
};
