import React from 'react';

import ReactSelect from 'react-select';
import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import {TextField, Button} from '@material-ui/core';

const MAX_NUM_OPTIONS = 100;

type ReactSelectOption = {
    label: string;
    value: string;
}

export type Props = {
    label: string;
    options?: ReactSelectOption[];
    onChange?: (value: string) => void;
    value?: string;
    onInputChange: (value: string) => void;
    inputValue: string;
    disabled?: boolean;
}

export default function SelectMui(props: Props) {
    const options = props.options || [];
    const handleChange = (e, value) => {
        if (props.onChange) {
            if (!value) {
                props.onChange('');
                return;
            }
            props.onChange(value.value);
            props.onInputChange('');
        }
    };

    const filterOptions = createFilterOptions({
        limit: MAX_NUM_OPTIONS,
        stringify: option => option.label,
    });

    const onInputChange = (event: object, value: string, reason: string) => {
        if(props.onInputChange && reason !== 'reset') {
            props.onInputChange(value);
        }
    }

    let inputValue = props.inputValue;
    if (!inputValue && props.value) {
        inputValue = options.find((opt) => opt.value === props.value).label;
    }

    const selectComponent = (
        <Autocomplete
            onChange={handleChange}
            value={options.find((opt) => opt.value === props.value) || null}
            filterOptions={filterOptions}
            onInputChange={onInputChange}
            inputValue={inputValue || ''}
            options={options}
            getOptionLabel={(option: ReactSelectOption) => option.label}
            disabled={props.disabled}
            noOptionsText={''}
            renderInput={params => (
                <TextField
                    {...params}
                    label={props.label}
                    variant={'outlined'}
                    placeholder={''}
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
            )}
            renderOption={opt => (
                <div style={{width: '100%'}}>
                    {' '} {opt.label}
                </div>
            )}
        />
    );

    return selectComponent;
}
