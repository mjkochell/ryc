import React from 'react';
import YouTube from 'react-youtube';
import {State, useStoreState} from 'easy-peasy';

import {WithInfo, Charity} from 'store/entities';
import {StoreModel} from 'store/store';

import {CharityFormState} from './charity_form_extra';

type Props = {
    charityForm: CharityFormState;
}

const renderWithInfo = (withInfo: WithInfo) => {
    let video;
    if (withInfo.videoURL) {
        const videoID = withInfo.videoURL.split('=').pop();
        video = (
            <YouTube
                videoId={videoID}
                opts={{
                    width: '480',
                    height: '270',
                }}
                onReady={console.log}
            />
        );
    }

    let website;
    if (withInfo.website) {
        website = (
            <p>
                <a href={withInfo.website}>{withInfo.website}</a>
            </p>
        );
    }

    let logo;
    if (withInfo.logo) {
        logo = (
            <a href={withInfo.website}>
                <img
                    style={styles.img}
                    src={withInfo.logo}
                />
            </a>
        );
    }

    return (
        <div>
            {website}
            {logo}
            {video}
        </div>
    );
};

export default function CharityShowcase(props: Props) {
    const {charityForm} = props;

    const parentCharities = useStoreState((state: State<StoreModel>) => state.charities.parentCharities);
    const getCharityLocations = useStoreState((state: State<StoreModel>) => state.charities.getLocationsByParentID);

    if (!charityForm.charity) {
        return <div/>;
    }

    const charity = parentCharities.find((c) => c.id === charityForm.charity) as Charity;

    if (!charityForm.charityLocation) {
        return renderWithInfo(charity);
    }

    const charityLocation = getCharityLocations(charityForm.charity).find((loc) => loc.id === charityForm.charityLocation);
    if (!charityLocation) {
        return renderWithInfo(charity);
    }

    return renderWithInfo({
        videoURL: charityLocation.videoURL || charity.videoURL,
        logo: charityLocation.logo || charity.logo,
        website: charityLocation.website || charity.website,
    });
}

const styles = {
    img: {

        // position: 'absolute',
        // top: 0,
    },
};
