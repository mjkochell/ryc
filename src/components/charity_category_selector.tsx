import React, {useState} from 'react';
import {useStoreState, State} from 'easy-peasy';

import {StoreModel} from 'store/store';

import {CharityFormState} from './charity_form';
import {MerchantFormState} from './merchant_form';
import Select from './select_mui';

type SIC = {
    code: string;
    description: string;
    sics: SIC1[];
};

type SIC1 = {
    code: string;
    description: string;
};

type Props = {
    charityForm: CharityFormState;
}

export default function CharityCategorySelector({charityForm}: Props) {
    const {selectedPrimarySIC, setPrimarySIC, selectedSIC1, setSIC1} = charityForm;

    const charities = useStoreState((state: State<StoreModel>) => state.charities.parentCharities);
    const filtered = charities.reduce((accum, entity) => {
        for (const loc of entity.locations) {
            const {'Primary SIC Code': sic, 'Primary SIC Code Description': sicDesc, 'SIC Code 1': sic1, 'SIC Code 1 Description': sic1Desc} = loc.csvData;
            accum[sic] = accum[sic] || {code: sic, description: sicDesc, sics: []} as SIC;

            if (!(accum[sic] as SIC).sics.find((s) => s.code === sic1)) {
                accum[sic].sics.push({code: sic1, description: sic1Desc});
            }

            return accum;
        }
    }, {});

    const parents = Object.values(filtered) as SIC[];
    let children: SIC1[] = [];
    if (selectedPrimarySIC) {
        children = filtered[selectedPrimarySIC].sics as SIC1[];
        const hash = {};
        for (const sic of children) {
            hash[sic.description] = hash[sic.description] || sic;
        }
        children = Object.values(hash);
    }

    const safeSetSIC1 = (value: string) => {
        const sics = filtered[selectedPrimarySIC].sics as SIC1[];
        const sic = sics.find((s) => s.code === value);
        const allMatches = children.filter(c => c.description === sic.description);
        console.log(allMatches);
        setSIC1(value);
    };

    const options = parents.map((c) => ({label: c.description, value: c.code}));

    const onChange = (value) => {
        setPrimarySIC(value);
        setSIC1('');
    };

    return (
        <div>
            <Select
                label={'Charity Category'}
                value={selectedPrimarySIC}
                options={options}
                onChange={onChange}
            />
            {/* <select
                value={selectedPrimarySIC}
                onChange={onChange}
            >
                {parents.map((c) => (
                    <option value={c.code} key={c.code}>
                        {c.description}
                    </option>
                ))}
            </select> */}
            {/* <select value={selectedSIC1} disabled={!children.length} onChange={e => safeSetSIC1(e.target.value)}>
                {children.map((s) => (
                    <option value={s.code} key={s.code}>
                        {s.description}
                    </option>
                ))}
            </select> */}
        </div>
    );
}
