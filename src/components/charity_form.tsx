import React from 'react';
import {useStoreState, State} from 'easy-peasy';

import MenuItem from '@material-ui/core/MenuItem';
import Input from '@material-ui/core/Input';

import {StoreModel} from 'store/store';
import {SourceEntity, Entity, CharityLocation, Charity} from 'store/entities';

import Select, {Props as SelectProps} from './select_mui';
import CharityCategorySelector from './charity_category_selector';
import {TextField} from '@material-ui/core';

export type CharityFormState = {
    charity: string;
    setCharity: (charityID: string) => void;
    charityText: string;
    setCharityText: (text: string) => void;
    charityLocation: string;
    setCharityLocation: (locationID: string) => void;
    charityLocationText: string;
    setCharityLocationText: (locationID: string) => void;
    charityGroup: string;
    setCharityGroup: (group: string) => void;
    fromQRCode: boolean;
    setFromQRCode: (from: boolean) => void;
    selectedPrimarySIC: string;
    setPrimarySIC: (sic: string) => void;
    selectedSIC1: string;
    setSIC1: (sic: string) => void;
};

type Props = {
    charityForm: CharityFormState;
}

export default function CharityForm(props: Props) {
    const {charityForm} = props;

    const parentCharities = useStoreState((state: State<StoreModel>) => state.charities.parentCharities);
    const getCharityLocations = useStoreState((state: State<StoreModel>) => state.charities.getLocationsByParentID);
    const getCharityGroups = useStoreState((state: State<StoreModel>) => state.charities.getGroupsByParentIDAndLocationID);

    const setParentCharity = (value: string) => {
        charityForm.setCharity(value);
        const locs = getCharityLocations(value);
        if (locs.length === 1) {
            selectCharityLocation(locs[0].id);
        } else {
            selectCharityLocation('');
        }
    };

    const selectCharityLocation = (value: string) => {
        charityForm.setCharityLocation(value);
        charityForm.setCharityGroup('');
    };

    const selectCharityGroup = (value: string) => {
        charityForm.setCharityGroup(value);
    };

    const charityOptions = parentCharities.map((charity: Charity) => ({
        label: charity.name,
        value: charity.id,
    }));

    window.charityForm = charityForm;
    const charityProps = {label: 'Charity', inputValue: charityForm.charityText, onInputChange: charityForm.setCharityText}
    let charitySelect = <Select {...charityProps}/>;
    if (parentCharities.length) {
        charitySelect = (
            <Select
                {...charityProps}
                value={charityForm.charity}
                options={charityOptions}
                onChange={setParentCharity}
            />
        );
    }

    const charityLocationProps = {label: 'Charity Location', inputValue: charityForm.charityLocationText, onInputChange: charityForm.setCharityLocationText}
    let charityLocationSelect = <Select {...charityLocationProps}/>;
    if (charityForm.charity) {
        const charityLocations = getCharityLocations(charityForm.charity);
        const locationOptions = charityLocations.map((loc) => ({
            label: loc.address,
            value: loc.id,
        }));

        charityLocationSelect = (
            <Select
                {...charityLocationProps}
                value={charityForm.charityLocation}
                options={locationOptions}
                onChange={selectCharityLocation}
            />
        );
    }

    let charityGroupSelect = (
        <TextField
            // {...params}
            style={{width: '100%'}}
            value={charityForm.charityGroup}
            onChange={({target: {value}}) => charityForm.setCharityGroup(value)}
            label={'Charity Group (optional)'}
            variant={'outlined'}
            placeholder={''}
            InputLabelProps={{
                shrink: true,
            }}
        />
    );


    // need to filter on selected sic code, and be sure to include currently selected item too if not in category

    return (
        <div>
            <div>
                {/* <CharityCategorySelector charityForm={charityForm}/> */}
            </div>
            <div>
                {charitySelect}
            </div>
            <div>
                {charityLocationSelect}
            </div>
            <div>
                {charityGroupSelect}
            </div>
        </div>
    );
}
