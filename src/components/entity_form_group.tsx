import React from 'react';

import ReactSelect from 'react-select';
import {useStoreState, State} from 'easy-peasy';

import {StoreModel} from 'store/store';

import {Entity} from 'store/entities';

import LimitedSelect from './select';

type Props = {
    entities: Entity[];
    selectedName: string;
    setName: (name: string) => void;
    locations: Entity[];
    selectedLocation: string;
    setLocation: (entityID: string) => void;
}

export default function EntityFormGroup(props: Props) {
    const nameOptions = props.entities.map((e) => ({
        label: e.name,
        value: e.id,
    }));

    const locationOptions = props.locations.map((loc) => ({
        label: loc.name,
        value: loc.id,
    }));

    return (
        <div style={styles.container}>
            <LimitedSelect
                value={nameOptions.find(({value}) => value === props.selectedName)}
                options={nameOptions}
                onChange={props.setName}
            />

            <ReactSelect
                value={locationOptions.find(({value}) => value === props.selectedLocation)}
                options={locationOptions}
                onChange={({value}) => props.setLocation(value)}
            />
        </div>
    );
}

const styles = {
    container: {
        // paddingLeft: '20px',
        // marginTop: '100px',
        width: '400px',
        // border: '1 solid',
    },
};
