import {Charity, Merchant, CharityGroup} from './store/entities';

import {CSVOutputEntity, CSVOutputColumns} from './data/export-format/interfaces';

import charityData from './data/export-format/charities-short';
import merchantData from './data/export-format/merchants-short';

type idxMap = {[key in keyof CSVOutputEntity]: number}

export const parseEntitiesFromCSV = (csvData: string): CSVOutputEntity[] => {
    const [topRow, ...rows] = csvData.split('\n');
    const topRowHash = topRow.split(',').reduce((accum, col, idx) => {
        return {...accum, [col]: idx};
    }, {} as any as idxMap);

    if (!rows[rows.length - 1]) {
        rows.pop();
    }

    return rows.map((rowStr: string) => {
        const row = rowStr.split(',');
        const entity = {} as any as CSVOutputEntity;
        for (const col of CSVOutputColumns) {
            entity[col] = row[topRowHash[col]];
        }
        return entity;
    });
};

const getNestedCharities = (charityCSV: string) => {
    const charities = parseEntitiesFromCSV(charityCSV);

    const nestedCharitiesHash: {[name: string]: Charity} = {};
    for (const entity of charities) {
        const name = entity['Company Name'];
        nestedCharitiesHash[entity['Company Name']] = nestedCharitiesHash[entity['Company Name']] || {
            name: entity['Company Name'],
            id: entity['Company Name'],
            locations: [],
        };
        const parent = nestedCharitiesHash[entity['Company Name']];

        let groups: CharityGroup[] = [];
        if (entity['Sub Groups']) {
            groups = entity['Sub Groups'].split('|').map((group) => {
                return {
                    id: group,
                    parent_id: name,
                    location_id: entity.UUID,
                    name: group,
                };
            });
        }

        parent.locations.push({
            ...entity,
            id: entity.UUID,
            parent_id: name,
            name,
            address: entity.Address,
            groups,
            csvData: entity,
        });
    }

    return Object.values(nestedCharitiesHash);
};

const getNestedMerchants = (merchantCSV: string) => {
    const merchants = parseEntitiesFromCSV(merchantCSV);

    const nestedMerchantsHash: {[name: string]: Merchant} = {};
    for (const entity of merchants) {
        const name = entity['Company Name'];
        nestedMerchantsHash[entity['Company Name']] = nestedMerchantsHash[entity['Company Name']] || {
            name: entity['Company Name'],
            id: entity['Company Name'],
            locations: [],
        };
        const parent = nestedMerchantsHash[entity['Company Name']];
        parent.locations.push({
            ...entity,
            id: entity.UUID,
            parent_id: name,
            name,
            address: entity.Address,
            csvData: entity,
        });
    }

    return Object.values(nestedMerchantsHash);
};

export const getLocalCharities = () => {
    const nestedCharities = getNestedCharities(charityData);
    window.charities = nestedCharities;
    return nestedCharities;
};

const csvHost = 'https://d19ek5n8sk7fv5.cloudfront.net/csv';

export const getRemoteCharities = async () => {
    const data = await fetch(csvHost + '/charities.csv').then(r => r.text());
    const nestedCharities = getNestedCharities(data);
    window.charities = nestedCharities;
    return nestedCharities;
};

export const getLocalMerchants = () => {
    const nestedMerchants = getNestedMerchants(merchantData);
    window.merchants = nestedMerchants;
    return nestedMerchants;
};

export const getRemoteMerchants = async () => {
    const data = await fetch(csvHost + '/merchants.csv').then(r => r.text());
    const nestedMerchants = getNestedMerchants(data);
    window.merchants = nestedMerchants;
    return nestedMerchants;
};
