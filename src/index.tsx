import React from 'react';
import ReactDOM from 'react-dom';
import {StoreProvider} from 'easy-peasy';

import store from './store/store';
import App from './app';

const contentInputSelector = '';
const formSelector = '';

const Main = () => {
    return (
        <StoreProvider store={store}>
            <App/>
        </StoreProvider>
    );
};

window.FormMain = Main;

window.React = React;
window.ReactDOM = ReactDOM;

window.addEventListener('load', () => {
    // const element = <Main/>;

    // ReactDOM.render(element, document.getElementById('main'));
});
