export function getFormContentInput(): HTMLTextAreaElement {
    const contentInputSelector = '#form-content-input';

    let input;
    const label = Array.from(document.querySelectorAll('label')).find((l) => l.innerText === 'Form Content');
    if (label) {
        const name = label.htmlFor;
        input = document.querySelector(`textarea[name="${name}"]`);
    } else {
        input = document.querySelector(contentInputSelector);
    }

    return input;
}

export function getFormContentInputLabel(): HTMLLabelElement {
    return Array.from(document.querySelectorAll('label')).find((l) => l.innerText === 'Form Content');
}

export function getGlobalForm(): HTMLFormElement {
    const label = getFormContentInputLabel();
    if (label) {
        return label.parentElement.parentElement;
    }
}

export function getGlobalFormSubmitButton(): HTMLButtonElement {
    const form = getGlobalForm();
    if (form) {
        return form.querySelector('div.dmformsubmit');
    }
}

export function getScriptAreaLabel(): HTMLButtonElement {
    return document.querySelector('.script-area');
}

export function getFormContainers() {
    return document.querySelectorAll('.innerPageTmplBox .dmRespRow');
}
