export const awsSubmit = async (form) => {
    return fetch(`${process.env.API_GATEWAY_URL}/submit_form`, {
        method: 'POST',
        body: JSON.stringify(form),
    });
};

export const railsSubmit = async (form) => {
    return fetch(`${process.env.RAILS_HOST}/submissions`, {
        method: 'POST',
        body: JSON.stringify({submission: form}),
        headers: {
            'Content-Type': 'application/json',
        },
    });
};

export const submitForm = railsSubmit;

export const ping = async () => {
    return fetch(`${process.env.RAILS_HOST}/ping`);
};
