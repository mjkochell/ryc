import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect,
} from 'react-router-dom';
import useReactRouter from 'use-react-router';
import {useStoreActions, Actions} from 'easy-peasy';

import {StoreModel} from './store/store';
// import {saveState} from './store/helpers';

import WebsiteForm from './components/website_form';
// import FileUpload from './components/file_upload';

import 'bootstrap-css-only/css/bootstrap-grid.min.css';
import './styles/styles';

const Header = () => {
    const {history} = useReactRouter();
    return (
        <h1>{history.location.pathname.slice(1)}</h1>
    );
};

export default function App() {
    const init = useStoreActions((dispatch: Actions<StoreModel>) => dispatch.init.init);

    React.useEffect(() => {
        init();
    }, []);

    return (
        <Router>
            {/* <button onClick={() => saveState({})}>
                {'Reset'}
            </button> */}
            <div>
                <WebsiteForm/>
                {/* <FileUpload/> */}
            </div>
        </Router>
    );
}
