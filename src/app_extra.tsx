import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect,
} from 'react-router-dom';
import useReactRouter from 'use-react-router';
import {useStoreActions, Actions} from 'easy-peasy';

import 'bootstrap-css-only/css/bootstrap-grid.css';

import {getFormContentInput, getFormContentInputLabel, getGlobalFormSubmitButton, getGlobalForm, getScriptAreaLabel, getFormContainers} from 'util/dom';

import {StoreModel} from './store/store';
// import {saveState} from './store/helpers';

import WebsiteForm from './components/website_form';
import FileUpload from 'components/file_upload';

const Header = () => {
    const {history} = useReactRouter();
    return (
        <h1>{history.location.pathname.slice(1)}</h1>
    );
};

export default function App() {
    const init = useStoreActions((dispatch: Actions<StoreModel>) => dispatch.init.init);
    React.useEffect(() => {
        init();

        const input = getFormContentInput();
        if (input) {
            input.style.display = 'none';
        }
        const label = getFormContentInputLabel();
        if (label) {
            label.style.display = 'none';
        }
        const button = getGlobalFormSubmitButton();
        if (button) {
            button.style.display = 'none';
        }

        const scriptPlaceholder = getScriptAreaLabel();
        if (scriptPlaceholder) {
            scriptPlaceholder.style.display = 'none';
        }

        const containers = getFormContainers();
        if (containers.length) {
            if (containers[0]) {containers[0].style.paddingBottom = 0;}
            if (containers[1]) {containers[1].style.paddingTop = 0;}
        }

        const form = getGlobalForm();
        if (form) {
            form.style.display = 'block !important';
        }
    }, []);

    return (
        <Router>
            {/* <button onClick={() => saveState({})}>
                {'Reset'}
            </button> */}
            <div>
                {/* <WebsiteForm/> */}
                <FileUpload/>
            </div>
        </Router>
    );
}
